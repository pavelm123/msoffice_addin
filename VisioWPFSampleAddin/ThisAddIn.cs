﻿//-----------------------------------------------------------------------
// 
//  Copyright (C) Microsoft Corporation.  All rights reserved.
// 
// THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Visio = Microsoft.Office.Interop.Visio;
using Office = Microsoft.Office.Core;

namespace VisioWPFSampleAddin
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            ShowWinFormOptions();

            ShowWPFOptions();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion

        private void ShowWPFOptions()
        {
            OptionsWindow window = new OptionsWindow();

            // use WindowInteropHelper to set the Owner of our WPF window to the Visio application window
            System.Windows.Interop.WindowInteropHelper hwndHelper = new System.Windows.Interop.WindowInteropHelper(window);
            hwndHelper.Owner = new IntPtr(Globals.ThisAddIn.Application.WindowHandle32);

            // show our window
            window.ShowDialog();

            // if OK was selected then do work
            if (window.DialogResult.HasValue && window.DialogResult.Value)
            {
                // do any work based on the success of the DialogResult property
            }
        }

        private void ShowWinFormOptions()
        {
            using (OptionsForm frm = new OptionsForm())
            {
                //

                // show our window
                // specifies the parent window using the IWin32Window interface
                frm.ShowDialog(new WindowWrapper(new IntPtr(Globals.ThisAddIn.Application.WindowHandle32)));

                // if OK was selected then do work
                if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    // do any work based on the success of the DialogResult property
                }
            }
        }
    }

    /// <summary>
    /// Provides the Handle property from the IWin32Window interface that is needed to specify the parent
    /// for a WinForm that is not WinForms based.
    /// </summary>
    public class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        private IntPtr _hwnd;

        public WindowWrapper(IntPtr handle)
        {
            _hwnd = handle;
        }

        public IntPtr Handle
        {
            get { return _hwnd; }
        }
    }
}
