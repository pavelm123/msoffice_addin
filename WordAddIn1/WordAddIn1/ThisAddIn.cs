﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Office.Tools;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;


using System.Runtime.InteropServices;



//тут как добавить контролы в actions pane или task pane или form region:
//https://msdn.microsoft.com/en-us/library/bb772076.aspx

namespace WordAddIn1
{
    /// <summary>
    /// Provides the Handle property from the IWin32Window interface that is needed to specify the parent
    /// for a WinForm that is not WinForms based.
    /// </summary>
    public class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        private IntPtr _hwnd;

        public WindowWrapper(IntPtr handle)
        {
            _hwnd = handle;
        }

        public IntPtr Handle
        {
            get { return _hwnd; }
        }
    }

    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.DocumentBeforeSave +=
    new Word.ApplicationEvents4_DocumentBeforeSaveEventHandler(Application_DocumentBeforeSave);

            Application.WindowBeforeRightClick +=
                new Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(BeforeRightClick);

            Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneDocumentActions].Visible = false;
            //or alternatively: Application.CommandBars["Task Pane"].Visible = false;
            //var aw = Application.ActiveWindow.;

        }


        private void ShowWPFOptions()
        {
            Window1 window = new Window1();

            // use WindowInteropHelper to set the Owner of our WPF window to the Visio application window
            System.Windows.Interop.WindowInteropHelper hwndHelper = new System.Windows.Interop.WindowInteropHelper(window);
            hwndHelper.Owner = new IntPtr(Globals.ThisAddIn.Application.ActiveWindow.Hwnd); //было  WindowHandle32 
	//еще один вариант: helper.Owner = Process.GetCurrentProcess().MainWindowHandle;
            
            // show our window
            window.ShowDialog();

            // if OK was selected then do work
            if (window.DialogResult.HasValue && window.DialogResult.Value)
            {
                // do any work based on the success of the DialogResult property
            }
        }

        /*
        private void displayMyWindow(Microsoft.Office.Interop.Word.Application application)
        {
            //это в excel надо было, чтобы фокус не переводился обратно в excel
            Globals.ThisAddIn.Application.Interactive = false;


            IntPtr wnd = new IntPtr(0);
            object window = application.ActiveWindow;
            if (window != null)
            {
                IOleWindow oleWindow = window as IOleWindow;
                if (oleWindow != null)
                {
                    oleWindow.GetWindow(out wnd);
                }
            }
        //...

        if (wnd != IntPtr.Zero)
            {
                WindowInteropHelper helper = new WindowInteropHelper(archiveSettingWindow);
                helper.Owner = wnd;
                archiveSettingWindow.ShowInTaskbar = false;
            }

            Globals.ThisAddIn.Application.Interactive = true;
        }*/

        /*
Clearing the Actions Pane When the Document is Opened
If the user saves the document while the actions pane is visible, the actions pane is visible every time the document is opened, whether or not the actions pane contains any controls. If you want to control when it appears, call the Clear method of the ActionsPane field in the Startup event handler of ThisDocument or ThisWorkbook to ensure that the actions pane is not visible when the document is opened.        */

            /*
        private MyUserControl myUserControl1 = new MyUserControl();
        myCustomTaskPane = this.CustomTaskPanes.Add(myUserControl1, "My Task Pane");
myCustomTaskPane.Visible = true;;
        private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        private void InitCustomTaskPane()
        {
            myUserControl1 = new MyUserControl();
            myCustomTaskPane = this.CustomTaskPanes.Add(myUserControl1,
                "New Task Pane");

            myCustomTaskPane.DockPosition =
                Office.MsoCTPDockPosition.msoCTPDockPositionFloating;
            myCustomTaskPane.Height = 500;
            myCustomTaskPane.Width = 500;

            myCustomTaskPane.DockPosition =
                Office.MsoCTPDockPosition.msoCTPDockPositionRight;
            myCustomTaskPane.Width = 300;

            myCustomTaskPane.Visible = true;
            myCustomTaskPane.DockPositionChanged +=
                new EventHandler(myCustomTaskPane_DockPositionChanged);
        }

        private void myCustomTaskPane_DockPositionChanged(object sender, EventArgs e)
        {
            Microsoft.Office.Tools.CustomTaskPane taskPane =
                sender as Microsoft.Office.Tools.CustomTaskPane;

            if (taskPane != null)
            {
                // Adjust sizes of user control and flow panel to fit current task pane size.
                MyUserControl userControl = taskPane.Control as MyUserControl;
                System.Drawing.Size paneSize = new System.Drawing.Size(taskPane.Width, taskPane.Height);
                userControl.Size = paneSize;
                userControl.FlowPanel.Size = paneSize;

                // Adjust flow direction of controls on the task pane.
                if (taskPane.DockPosition ==
                    Office.MsoCTPDockPosition.msoCTPDockPositionTop ||
                    taskPane.DockPosition ==
                    Office.MsoCTPDockPosition.msoCTPDockPositionBottom)
                {
                    userControl.FlowPanel.FlowDirection =
                        System.Windows.Forms.FlowDirection.LeftToRight;
                }
                else
                {
                    userControl.FlowPanel.FlowDirection =
                        System.Windows.Forms.FlowDirection.TopDown;
                }
            }
        }
        */

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        void BeforeRightClick(Word.Selection sel, ref bool Cancel)
        {
            //Document.ActionsPane.Controls.Add(actions);

            ShowWPFOptions();

            StringBuilder list = new StringBuilder();
            var e = Application.AddIns.GetEnumerator();
            list.Append(Application.Path + Environment.NewLine);
            list.Append(Application.StartupPath + Environment.NewLine);

            //Globals.Ribbons.myRibbon.myButton.Enabled = true;
            do
            {
                Word.AddIn addin = e.Current as Word.AddIn;
                if (addin!=null) list.Append($"{addin.Name} - {addin.Path}");
            } while (e.MoveNext());
            sel.Paragraphs[1].Range.InsertParagraphBefore();
            sel.Paragraphs[1].Range.Text = list.ToString();

            /*
            Word.Range rng = Doc.Range(0, 0);
            rng.Text = "New Text";
            rng.Select();
            */
        }


        void Application_DocumentBeforeSave(Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            Doc.Paragraphs[1].Range.InsertParagraphBefore();
            Doc.Paragraphs[1].Range.Text = "This text was added by using code.";
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
