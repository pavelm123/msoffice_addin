﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Word2013_2016_Addin;

namespace Word2013_2016_Addin_Tests
{
    [TestClass]
    public class LoggerTest
    {
        [TestMethod]
        public void TestLogString()
        {
            var memoryStream = new MemoryStream();
            string testMessage = "Test Message";
            Logger logger = new Logger(new StreamWriter(memoryStream));
            logger.Log(testMessage);
            memoryStream.Seek(0, SeekOrigin.Begin);
            StreamReader reader = new StreamReader(memoryStream);
            string logMessage = reader.ReadToEnd();
            Assert.IsTrue(logMessage.Contains(testMessage));
        }

        [TestMethod]
        public void TestLogException()
        {
            var memoryStream = new MemoryStream();
            string testMessage = "Test Message";
            Exception exception = new Exception(testMessage);
            Logger logger = new Logger(new StreamWriter(memoryStream));
            logger.Log(exception);
            memoryStream.Seek(0, SeekOrigin.Begin);
            StreamReader reader = new StreamReader(memoryStream);
            string logMessage = reader.ReadToEnd();
            Assert.IsTrue(logMessage.Contains(testMessage));
            Assert.IsTrue(logMessage.StartsWith("ERROR! "));
        }
    }
}
