﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Word2013_2016_Addin;
using System.IO;
using System.Net;

namespace Word2013_2016_Addin_Tests
{
    [TestClass]
    public class RequestSenderTest
    {
        [TestMethod]
        public void TestGetFormattedRequestStringCorrectFormat()
        {
            var memoryStream = new MemoryStream();
            Logger logger = new Logger(new StreamWriter(memoryStream));
            RequestSender requestSender = new RequestSender(logger);

            requestSender.RequestParameters.Add("Param1", "Value1");
            requestSender.RequestParameters.Add("Param2", "Value2");

            Assert.AreEqual("Param1=Value1&Param2=Value2", requestSender.GetFormattedRequestString());
        }

        [TestMethod]
        public void TestLoggerMessagesNotFound()
        {
            var memoryStream = new MemoryStream();
            Logger logger = new Logger(new StreamWriter(memoryStream));
            RequestSender requestSender = new RequestSender(logger);

            var results = requestSender.GetResponseResults(WebRequest.Create("http://localhost/123123123") as HttpWebRequest);
            StreamReader reader = new StreamReader(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);

            Assert.IsTrue(reader.ReadToEnd().Contains("404"));
        }

        [TestMethod]
        public void TestParseResponseStreamCorrectOutput()
        {
            var memoryStream = new MemoryStream();
            Logger logger = new Logger(new StreamWriter(memoryStream));
            RequestSender requestSender = new RequestSender(logger);
            //json stream arrange
            string jsonToParse = @"[{""Authors"":""Author1"",""ID"":1,""JournalTitle"":""Manure"",""ProductName"":""DoggiePoo"",""PublishDate"":""2012-04-23T18:25:43.511Z"",""TradeName"":""Trade1""},{""Authors"":""Author2"",""ID"":2,""JournalTitle"":""Juice"",""ProductName"":""Yes"",""PublishDate"":""2015-08-23T18:05:43.511Z"",""TradeName"":""Trade2""}]";
            MemoryStream paramStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(paramStream);
            writer.Write(jsonToParse);
            writer.Flush();
            paramStream.Seek(0, SeekOrigin.Begin);

            var parsedItems = requestSender.ParseResponseStream(paramStream);

            Assert.IsTrue(parsedItems.Count == 2);
            Assert.IsTrue(parsedItems[0].ID == 1);
            Assert.IsTrue(parsedItems[1].Authors == "Author2");
        }

        /*[TestMethod]
        public void TestToFail()
        {
            Assert.IsTrue(false);
        }*/
    }
}
