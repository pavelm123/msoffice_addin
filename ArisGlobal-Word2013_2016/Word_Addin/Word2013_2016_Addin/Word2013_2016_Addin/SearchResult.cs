﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Word2013_2016_Addin
{
    public class SearchResult
    {
        public int ID { get; set; }
        public string Authors { get; set; }
        public string ProductName { get; set; }
        public string JournalTitle { get; set; }
        public string PublishDate { get; set; }
        public string TradeName { get; set; }
    }

}
