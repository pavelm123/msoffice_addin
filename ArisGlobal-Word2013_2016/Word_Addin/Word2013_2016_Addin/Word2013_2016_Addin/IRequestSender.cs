﻿using System.Collections.Generic;
using System.Net;

namespace Word2013_2016_Addin
{
    interface IRequestSender
    {
        List<SearchResult> GetResponseResults(HttpWebRequest request);
    }
}