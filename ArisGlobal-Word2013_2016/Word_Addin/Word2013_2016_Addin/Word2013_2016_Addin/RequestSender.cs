﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace Word2013_2016_Addin
{
    public class RequestSender : IRequestSender
    {
        public RequestSender(Logger logger)
        {
            RequestParameters = new Dictionary<string, string>();
            _logger = logger;
        }

        public Dictionary<string, string> RequestParameters { get; set; }

        private Logger _logger { get; set; }

        public string GetFormattedRequestString()
        {
            string result = "";
            bool isFirst = true;
            foreach (var requestParam in RequestParameters)
            {
                if (!isFirst)
                    result += "&";
                else
                    isFirst = false;

                result += string.Format("{0}={1}", requestParam.Key, requestParam.Value);
            }
            return result;
        }
        public List<SearchResult> GetResponseResults(HttpWebRequest request)
        {
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        _logger.Log(new Exception(String.Format(
                                                                        "Server error (HTTP {0}: {1}).",
                                                                        response.StatusCode,
                                                                        response.StatusDescription)));
                        return null;
                    }
                    return ParseResponseStream(response.GetResponseStream());
                }

            }
            catch (Exception e)
            {
                _logger.Log(e);
            }
            return null;
        }

        public List<SearchResult> ParseResponseStream(Stream stream)
        {
            try
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<SearchResult>));
                object objResponse = jsonSerializer.ReadObject(stream);
                List<SearchResult> jsonResponse = objResponse as List<SearchResult>;
                return jsonResponse;
            }
            catch (Exception e)
            {
                _logger.Log(e);
            }
            return null;
        }
    }
}
