﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Word2013_2016_Addin
{
    public class Logger
    {
        private StreamWriter _writer;

        public Logger(StreamWriter writer)
        {
            _writer = writer;
        }

        public void Log(string message)
        {
            try
            {
                _writer.WriteLine(string.Format("[{0}] {1}", DateTime.Now, message));
                _writer.Flush();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void Log(Exception exception)
        {
            try
            {
                _writer.WriteLine(string.Format("ERROR! [{0}] {1}", DateTime.Now, exception.Message));
                _writer.WriteLine(string.Format("\t Inner Exception: {0}", DateTime.Now, exception.InnerException));
                _writer.WriteLine(string.Format("\t Stack Trace:", DateTime.Now, exception.StackTrace));
                _writer.Flush();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
