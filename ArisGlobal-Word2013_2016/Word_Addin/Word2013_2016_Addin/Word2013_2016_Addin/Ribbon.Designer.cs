﻿namespace Word2013_2016_Addin
{
    partial class Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ribbon));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpMainGroup = this.Factory.CreateRibbonGroup();
            this.btnShowWindow = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpMainGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpMainGroup);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // grpMainGroup
            // 
            this.grpMainGroup.Items.Add(this.btnShowWindow);
            this.grpMainGroup.Label = "agInquirer QnC plugin";
            this.grpMainGroup.Name = "grpMainGroup";
            // 
            // btnShowWindow
            // 
            this.btnShowWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnShowWindow.Image")));
            this.btnShowWindow.Label = "Show Window";
            this.btnShowWindow.Name = "btnShowWindow";
            this.btnShowWindow.ShowImage = true;
            this.btnShowWindow.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnShowWindow_Click);
            // 
            // Ribbon
            // 
            this.Name = "Ribbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpMainGroup.ResumeLayout(false);
            this.grpMainGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpMainGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnShowWindow;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon Ribbon
        {
            get { return this.GetRibbon<Ribbon>(); }
        }
    }
}
