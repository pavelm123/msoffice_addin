﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Word2013_2016_Addin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logger _logger;
        private List<SearchResult> _searchResults = new List<SearchResult>();

        public SearchResult SearchResult {
            get {
                return dgSearchResult.SelectedIndex == -1? null: _searchResults[dgSearchResult.SelectedIndex];
            }
        }


        public MainWindow(Logger logger)
        {
            this._logger = logger;
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                _logger.Log("Saving data to registry");
                string registryKey = string.Format("{0}\\{1}", Properties.Settings.Default.RegistryRoot, Properties.Settings.Default.RegistryKey);
                Registry.SetValue(registryKey, "WindowPositionX", Left);
                Registry.SetValue(registryKey, "WindowPositionY", Top);
                Registry.SetValue(registryKey, "WindowWidth", Width);
                Registry.SetValue(registryKey, "WindowHeight", Height);
            }
            catch (Exception ex)
            {
                _logger.Log("Failed to save data to registry");
                _logger.Log(ex);
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {
                _logger.Log("Reading data from registry");
                string registryKey = string.Format("{0}\\{1}", Properties.Settings.Default.RegistryRoot, Properties.Settings.Default.RegistryKey);
                Width = Convert.ToDouble(Registry.GetValue(registryKey, "WindowWidth", Properties.Settings.Default.DefaultWidth));
                Height = Convert.ToDouble(Registry.GetValue(registryKey, "WindowHeight", Properties.Settings.Default.DefaultHeight));
                Left = Convert.ToDouble(Registry.GetValue(registryKey, "WindowPositionX", System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2 - Width / 2));
                Top = Convert.ToDouble(Registry.GetValue(registryKey, "WindowPositionY", System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height / 2 - Height / 2));
            }
            catch (Exception ex)
            {
                _logger.Log("Failed to read data from registry");
                _logger.Log(ex);
            }

        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            RequestSender requestSender = new RequestSender(_logger);
            requestSender.RequestParameters.Add("ID", tbID.Text);
            requestSender.RequestParameters.Add("Authors", tbAuthors.Text);
            requestSender.RequestParameters.Add("JournalTitle", tbJournalTitle.Text);
            requestSender.RequestParameters.Add("ProductName", tbProductName.Text);
            requestSender.RequestParameters.Add("TradeName", tbTradeName.Text);
            requestSender.RequestParameters.Add("PublishDate", dtPublishDate.Text);

            _searchResults = requestSender.GetResponseResults(WebRequest.Create(Properties.Settings.Default.ServicePath) as HttpWebRequest);
            dgSearchResult.ItemsSource = _searchResults;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            tbID.Text = string.Empty;
            tbAuthors.Text = string.Empty;
            tbJournalTitle.Text = string.Empty;
            tbProductName.Text = string.Empty;
            tbTradeName.Text = string.Empty;
            dtPublishDate.Text = string.Empty;

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
