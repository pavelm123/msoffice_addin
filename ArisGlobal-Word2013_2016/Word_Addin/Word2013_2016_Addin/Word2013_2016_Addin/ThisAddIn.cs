﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;
using System.Windows.Controls;

namespace Word2013_2016_Addin
{
    public partial class ThisAddIn
    {
        private Logger logger;
        Office._CommandBarButtonEvents_ClickEventHandler eventHandler;

        //Event Handler for the button click
        public void MyButton_Click(Office.CommandBarButton cmdBarbutton, ref bool cancel)
        {
            ShowSearchWindow();
            cancel = true;
        }


        public void ShowSearchWindow()
        {
            var searchWindow = new MainWindow(logger);
            if (searchWindow.ShowDialog().Value)
                PasteText(searchWindow.SearchResult);
        }

        private void PasteText(SearchResult searchResult)
        {
            if (searchResult == null)
                return;

            Application.Selection.InsertAfter(string.Format("{0} | {1} | {2} | {3} | {4} | {5}", searchResult.ID, searchResult.JournalTitle, searchResult.ProductName, searchResult.PublishDate, searchResult.TradeName, searchResult.PublishDate));
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            logger = new Logger(new System.IO.StreamWriter(Properties.Settings.Default.LogFile));

            try
            {
                eventHandler = new Office._CommandBarButtonEvents_ClickEventHandler(MyButton_Click);
                Word.Application applicationObject = Globals.ThisAddIn.Application as Word.Application;
                applicationObject.WindowBeforeRightClick += new Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(App_WindowBeforeRightClick);
            }
            catch (Exception exception)
            {
                logger.Log(exception);
            }
        }

        private void App_WindowBeforeRightClick(Word.Selection Sel, ref bool Cancel)
        {
            try
            {
                this.AddItem();
            }
            catch (Exception exception)
            {
                logger.Log(exception);
            }
        }
        private void AddItem()
        {
            Word.Application applicationObject = Globals.ThisAddIn.Application as Word.Application;
            Office.CommandBarButton commandBarButton = applicationObject.CommandBars.FindControl(Office.MsoControlType.msoControlButton, missing, "HELLO_TAG", missing) as Office.CommandBarButton;
            if (commandBarButton != null)
            {
                logger.Log("Found button, attaching handler");
                commandBarButton.Click += eventHandler;
                return;
            }
            Office.CommandBar popupCommandBar = applicationObject.CommandBars["Text"];
            bool isFound = false;
            foreach (object _object in popupCommandBar.Controls)
            {
                Office.CommandBarButton _commandBarButton = _object as Office.CommandBarButton;
                if (_commandBarButton == null) continue;
                if (_commandBarButton.Tag.Equals("HELLO_TAG"))
                {
                    isFound = true;
                    logger.Log("Found existing button. Will attach a handler.");
                    _commandBarButton.Caption = "Hello !!!";
                    _commandBarButton.FaceId = 356;
                    _commandBarButton.Tag = "HELLO_TAG";
                    _commandBarButton.BeginGroup = true;
                    _commandBarButton.Click += eventHandler;
                    break;
                }
            }
            if (!isFound)
            {
                commandBarButton = (Office.CommandBarButton)popupCommandBar.Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, missing, true);
                logger.Log("Created new button, adding handler");
                commandBarButton.Click += eventHandler;
                commandBarButton.Caption = "Hello !!!";
                commandBarButton.FaceId = 356;
                commandBarButton.Tag = "HELLO_TAG";
                commandBarButton.BeginGroup = true;
            }
        }

        private void RemoveItem()
        {
            Word.Application applicationObject = Globals.ThisAddIn.Application as Word.Application;
            Office.CommandBar popupCommandBar = applicationObject.CommandBars["Text"];
            foreach (object _object in popupCommandBar.Controls)
            {
                Office.CommandBarButton commandBarButton = _object as Office.CommandBarButton;
                if (commandBarButton == null) continue;
                if (commandBarButton.Tag.Equals("HELLO_TAG"))
                {
                    popupCommandBar.Reset();
                }
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            Word.Application App = Globals.ThisAddIn.Application as Word.Application;
            App.WindowBeforeRightClick -= new Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(App_WindowBeforeRightClick);
        }


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
